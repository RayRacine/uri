#lang info
(define collection "uri")
(define deps '("string-util"
               "opt"
               "typed-racket-lib"
               "base"))
(define build-deps '("scribble-lib"
                     "racket-doc"
                     "rackunit-lib"))
(define scribblings '(("scribblings/uri.scrbl" ())))
(define pkg-desc "URI and various schemes such as URL in Typed Racket.")
(define version "0.1.0")
(define pkg-authors '("Raymond Racine"))
